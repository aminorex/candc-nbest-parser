// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/_printer.h"
#include "parser/print_ccgbank.h"


using namespace std;

namespace NLP { namespace CCG {

void
CCGbankPrinter::header(const std::string &PREFACE){
  out.stream << PREFACE << endl;
  log.stream << PREFACE << endl;
}

void
CCGbankPrinter::unary(Sentence &sent){
  out.stream << "(<L " << sent.msuper[0][0].raw << ' ';
  out.stream << sent.pos[0] << ' ' << sent.pos[0];
  out.stream << sent.msuper[0][0].raw << ">)" << endl;
}

void
CCGbankPrinter::recurse(kBestSC *ksc, Sentence &sent){
  const SuperCat *sc = ksc->sc;
  if(sc->left){
    assert(!sc->left->kmax.empty());
    out.stream << "(<T ";
    sc->cat->out_novar_noX(out.stream, false, sc->conj());
    out.stream << ' ' << sc->flags2str(); // line added by Siva Reddy

    //JC+DNG: fixing dodgy head hack
    int head = 0;
    int nchildren = 1;
    if(sc->right){
      nchildren = 2;
      if(!(sc->right->kmax.at((int)ksc->rightRank).sc->vars[Vars::X] != sc->vars[Vars::X]))
	head = 1;
    }

    out.stream << ' ' << head << ' ' << nchildren << "> ";
    recurse(&sc->left->kmax.at((int)ksc->leftRank), sent);

    if(sc->right){
      out.stream << ' ';
      assert(!sc->right->kmax.empty());
      recurse(&sc->right->kmax.at((int)ksc->rightRank), sent);
    }
    out.stream << ')';
  }else{
    // leaf case (<L NP[nb]/N DT DT the NP[nb]_131/N_131>)
    Position pos = (sc->vars[sc->cat->var]).pos();
    out.stream << "(<L ";
    sc->cat->out_novar_noX(out.stream, false) << ' ';
    out.stream << sent.words[pos - 1] << ' ';
    out.stream << sent.lemmas[pos - 1] << ' ';
    out.stream << sent.pos[pos - 1] << ' ' << sent.entities[pos - 1] << ' ' << sent.chunks[pos - 1] << ' ';
    sc->cat->out_novar(out.stream, false);
    out.stream << ">)";
  }
}

void
CCGbankPrinter::derivation(kBestSC *ksc, Sentence &sent){
  out.stream << "ID=" << nsentences << " PARSER=GOLD NUMPARSE=1" << endl;
  recurse(ksc, sent);
  out.stream << endl;
}

void
CCGbankPrinter::lexical(Sentence &){}

} }
