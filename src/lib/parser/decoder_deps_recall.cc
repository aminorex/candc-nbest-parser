// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/_decoder.h"
#include "parser/inside_outside.h"
#include "parser/decoder_deps_recall.h"

namespace NLP { namespace CCG {

DepsRecallDecoder::DepsRecallDecoder(void): inside_outside(new InsideOutside()) {}

DepsRecallDecoder::~DepsRecallDecoder(void){
  delete inside_outside;
}

std::vector<kBestSC> *
DepsRecallDecoder::best(Chart &chart){
  inside_outside->calc(chart);
  return Decoder::best(chart);
}

double
DepsRecallDecoder::best_score(const SuperCat *sc){
  double score = 0.0;

  if(sc->left){
    const SuperCat *b = best_equiv(sc->left);
    score += b->score;

    if(sc->right){
      const SuperCat *b = best_equiv(sc->right);
      score += b->score;
    }
  }

  for(const Filled *filled = sc->filled; filled; filled = filled->next)
    score += inside_outside->depscores[filled] / filled->conj;

  assert(score >= 0.0);
  return sc->score = score;
}

std::vector<kBestSC> *
DepsRecallDecoder::k_best_0(Chart &chart){
  Cell &root = chart.root();

  std::vector<kBestSC> *ret = new std::vector<kBestSC>();

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    std::vector<kBestSC> *current = k_best_0_equiv(*i, chart.K);
    Decoder::k_merge_0(ret, current, chart.K);
  }
  
  return ret;
}

std::vector<kBestSC>
DepsRecallDecoder::k_best_0_score(const SuperCat *sc, const double K){
  double score = sc->score;
  std::vector<kBestSC> ret;

  if(sc->left){
    std::vector<kBestSC> *l = k_best_0_equiv(sc->left, K);

    if(sc->right){
      std::vector<kBestSC> *r = k_best_0_equiv(sc->right, K);

      for(int i = 0; i < (int)l->size(); ++i) {
        for(int j = 0; j < (int)r->size(); ++j) {
          kBestSC current = kBestSC(sc, i, j);
          current.score = score + (*l)[i].score + (*r)[j].score;
          ret.push_back(current);
        }
      }

      std::sort(ret.begin(), ret.end(), kBestSC::compare);
      if(ret.size() > K)
        ret.resize(K);
    }
    else {
      ret = *l;
      if(ret.size() > K)
        ret.resize(K);
    }
  }
  else {
    kBestSC kb = kBestSC(sc);
    kb.score = score;
    ret.push_back(kb);
  }
  
  return ret;
}

std::vector<kBestSC> *
DepsRecallDecoder::k_best_1(Chart &chart){
  Cell &root = chart.root();

  std::vector<kBestSC> *ret = new std::vector<kBestSC>();

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    std::vector<kBestSC> *current = k_best_0_equiv(*i, chart.K);
    Decoder::k_merge_0(ret, current, chart.K);
  }
  
  return ret;
}

std::vector<kBestSC>
DepsRecallDecoder::k_best_1_score(const SuperCat *sc, const double K){
  double score = sc->score;
  std::vector<kBestSC> ret;
  
  if(sc->left){
    std::vector<kBestSC> *l = k_best_1_equiv(sc->left, K);

    if(sc->right){
      std::vector<kBestSC> *r = k_best_1_equiv(sc->right, K);
      
      std::multiset<kBestSC> cand;
      // Initialize the candidate set
      kBestSC temp = kBestSC(sc, 0, 0); 
      temp.score = score + (*l)[0].score + (*r)[0].score;
      cand.insert(temp);
      
      while(!cand.empty() && ret.size() < K){
        // Add best-scoring kBestSC to ret
        kBestSC current = *cand.begin();
        ret.push_back(current);
        
        // Remove it from the candidate set
        cand.erase(cand.begin());
        
        // Add the neighbors of that to cand
        if(current.leftRank + 1 < l->size()) {
          kBestSC next = kBestSC(sc, current.leftRank + 1, current.rightRank);
          next.score = score + (*l)[current.leftRank + 1].score + (*r)[current.rightRank].score;
          // Check to see if it's already in the multiset
          if(cand.count(next) == 0)
            cand.insert(next);
        }
        if(current.rightRank + 1 < r->size()) {
          kBestSC next = kBestSC(sc, current.leftRank, current.rightRank + 1);
          next.score = score + (*l)[current.leftRank].score + (*r)[current.rightRank + 1].score;
          // Check to see if it's already in the multiset
          if(cand.count(next) == 0)
            cand.insert(next);
        }
      }
    }
    else {
      ret = *l;
      // TODO: size should already be no greater than K - confirm
      if(ret.size() > K)
        ret.resize(K);
    }
  }
  else {
    // sc is a leaf node -- only need the score stored
    kBestSC kb = kBestSC(sc);
    kb.score = score;
    ret.push_back(kb);
  }
  
  return ret;
}

std::vector<kBestSC> *
DepsRecallDecoder::k_best_2(Chart &chart){
  Cell &root = chart.root();

  std::vector<kBestSC> ret;

  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    std::vector<kBestSC> *current = k_best_1_equiv(*i, chart.K);
    ret = k_merge_0(&ret, current, chart.K); // Merge is the same for this version, so re-use existing code
  }
  
  // TODO: This should never happen (merge returns at most k items) - confirm
  if(ret.size() > chart.K)
    ret.resize(chart.K);
  
  chart.kbest = ret;
  return &chart.kbest;
}

kBestSC
DepsRecallDecoder::k_best_2_score(const SuperCat *sc, const double K){
  double score = sc->score;
  kBestSC kb = kBestSC(sc, 0, 0);
  
  if(sc->left){
    std::vector<kBestSC> *l = k_best_2_equiv(sc->left, K);
    score += (*l)[0].score;

    if(sc->right){
      std::vector<kBestSC> *r = k_best_2_equiv(sc->right, K);     
      score += (*r)[0].score;
    }
  }
  
  kb.score = score;
  return kb;
}

std::vector<kBestSC> *
DepsRecallDecoder::k_best_3(Chart &chart){
  Cell &root = chart.root();
  
  std::multiset<kBestSC> cand;
  
  // Init candidate set
  for(Cell::iterator i = root.begin(); i != root.end(); ++i){
    cand.insert((*i)->kmax[0]);
  }
  
  while(chart.kbest.size() < chart.K){
    if(chart.kbest.size() > 0){
      kBestSC next = chart.kbest.at(chart.kbest.size() - 1);
      k_best_3_lazynext(cand, next, chart.K);
    }
    
    if(!cand.empty()){
      kBestSC temp = *cand.begin();
      chart.kbest.push_back(temp);
      cand.erase(cand.begin());
    }
    else
      break;
  }
  
  return &chart.kbest;
}

void
DepsRecallDecoder::k_best_3_lazynext(std::multiset<kBestSC> &cand, kBestSC kb, const double K){
  const SuperCat *sc = kb.sc;
  double score = sc->score;
  
  if(sc->left){
    k_best_3_lazy(sc->left, kb.leftRank + 1, K);
    std::vector<kBestSC> *l = &sc->left->kmax;
    
    if(sc->right){
      k_best_3_lazy(sc->right, kb.rightRank + 1, K);
      std::vector<kBestSC> *r = &sc->right->kmax;
      
      if(kb.leftRank + 1 < l->size()){
        kBestSC next = kBestSC(sc, kb.leftRank + 1, kb.rightRank);
        next.score = score + (*l)[kb.leftRank + 1].score + (*r)[kb.rightRank].score;
        // Check to see if it's already in the multiset
        if(cand.count(next) == 0)
          cand.insert(next);
      }
      
      if(kb.rightRank + 1 < r->size()){
        kBestSC next = kBestSC(sc, kb.leftRank, kb.rightRank + 1);
        next.score = score + (*l)[kb.leftRank].score + (*r)[kb.rightRank + 1].score;
        // Check to see if it's already in the multiset
        if(cand.count(next) == 0)
          cand.insert(next);
      }
      
    }
    else{ // Unary rule
      if(kb.leftRank + 1 < l->size()){
        kBestSC next = kBestSC(sc, kb.leftRank + 1, kb.rightRank);
        next.score = score + (*l)[kb.leftRank + 1].score;
        // Check to see if it's already in the multiset
        if(cand.count(next) == 0)
          cand.insert(next);
      }
      
    }
  }
  
}

void DepsRecallDecoder::k_best_3_getCandidates(const SuperCat *sc){
  
  for(const SuperCat *equiv = sc; equiv; equiv = equiv->next){
    kBestSC current = kBestSC(equiv, 0, 0);
    current.score = equiv->score;
    if(equiv->left) {
      current.score += equiv->left->kmax[0].score;
      if(equiv->right)
        current.score += equiv->right->kmax[0].score;
    }
    sc->cand.insert(current);
  }
  
  // Remove the 1-best parse, as it's already in kmax
  sc->cand.erase(sc->cand.begin());

}

} }
