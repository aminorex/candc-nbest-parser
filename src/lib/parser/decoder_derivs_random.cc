// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/_decoder.h"
#include "parser/decoder_derivs_random.h"

namespace NLP { namespace CCG {

double
DerivsRandomDecoder::best_score(const SuperCat *sc){
  if(sc->left){
    best_equiv(sc->left);

    if(sc->right)
      best_equiv(sc->right);
  }

  return sc->score = rand();
}

std::vector<kBestSC>
DerivsRandomDecoder::k_best_0_score(const SuperCat *sc, const double K){
  std::vector<kBestSC> ret;

  if(sc->left){
    k_best_0_equiv(sc->left, K);

    if(sc->right){
      k_best_0_equiv(sc->right, K);
    }
  }
   
  kBestSC kb = kBestSC(sc);
  kb.score = rand();
  ret.push_back(kb); 
  
  return ret;
}

std::vector<kBestSC>
DerivsRandomDecoder::k_best_1_score(const SuperCat *sc, const double K){
  std::vector<kBestSC> ret;

  if(sc->left){
    k_best_1_equiv(sc->left, K);

    if(sc->right){
      k_best_1_equiv(sc->right, K);
    }
  }
   
  kBestSC kb = kBestSC(sc);
  kb.score = rand();
  ret.push_back(kb); 
  
  return ret;
}

kBestSC
DerivsRandomDecoder::k_best_2_score(const SuperCat *sc, const double K){
  double score = sc->score;
  kBestSC kb = kBestSC(sc, 0, 0);
  
  if(sc->left){
    k_best_2_equiv(sc->left, K);

    if(sc->right){
      k_best_2_equiv(sc->right, K);
    }
  }
  
  kb.score = rand();
  return kb;
}

void
DerivsRandomDecoder::k_best_3_lazynext(std::multiset<kBestSC> &cand, kBestSC kb, const double K){
  const SuperCat *sc = kb.sc;
  double score = sc->score;
  
  if(sc->left){
    k_best_3_lazy(sc->left, kb.leftRank + 2, K);
    std::vector<kBestSC> *l = &sc->left->kmax;
    
    if(sc->right){
      k_best_3_lazy(sc->right, kb.rightRank + 2, K);
      std::vector<kBestSC> *r = &sc->right->kmax;
      
      if(kb.leftRank + 1 < l->size()){
        kBestSC next = kBestSC(sc, kb.leftRank + 1, kb.rightRank);
        next.score = rand();
        // Check to see if it's already in the multiset
        if(cand.count(next) == 0)
          cand.insert(next);
      }
      
      if(kb.rightRank + 1 < r->size()){
        kBestSC next = kBestSC(sc, kb.leftRank, kb.rightRank + 1);
        next.score = rand();
        // Check to see if it's already in the multiset
        if(cand.count(next) == 0)
          cand.insert(next);
      }
      
    }
    else{ // Unary rule
      if(kb.leftRank + 1 < l->size()){
        kBestSC next = kBestSC(sc, kb.leftRank + 1, kb.rightRank);
        next.score = rand();
        // Check to see if it's already in the multiset
        if(cand.count(next) == 0)
          cand.insert(next);
      }
      
    }
  }
  
}

void DerivsRandomDecoder::k_best_3_getCandidates(const SuperCat *sc){
  
  for(const SuperCat *equiv = sc; equiv; equiv = equiv->next){
    kBestSC current = kBestSC(equiv, 0, 0);
    current.score = rand();
    sc->cand.insert(current);
  }
  
  // Remove the 1-best parse, as it's already in kmax
  // This won't do anything interesting for the random version
  sc->cand.erase(sc->cand.begin());

}

} }
