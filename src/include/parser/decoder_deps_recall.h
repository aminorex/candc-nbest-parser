/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

namespace NLP {
  namespace CCG {

    class InsideOutside;

    class DepsRecallDecoder: public Decoder {
    public:
      InsideOutside *inside_outside;

      DepsRecallDecoder(void);
      virtual ~DepsRecallDecoder(void);

      virtual double best_score(const SuperCat *sc);
      virtual std::vector<kBestSC> *best(Chart &chart);
      
      virtual std::vector<kBestSC> k_best_0_score(const SuperCat *sc, const double K);
      virtual std::vector<kBestSC> *k_best_0(Chart &chart);
      
      virtual std::vector<kBestSC> k_best_1_score(const SuperCat *sc, const double K);
      virtual std::vector<kBestSC> *k_best_1(Chart &chart);
      
      virtual kBestSC k_best_2_score(const SuperCat *sc, const double K);
      virtual std::vector<kBestSC> *k_best_2(Chart &chart);
      
      virtual void k_best_3_getCandidates(const SuperCat *sc);
      virtual void k_best_3_lazynext(std::multiset<kBestSC> &cand, kBestSC kb, const double K);
      virtual std::vector<kBestSC> *k_best_3(Chart &chart);
    };
  }
}
