/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) Forrest Brennen
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

namespace NLP {
  namespace CCG {

    class SuperCat;

    // Used to create a k-best ranking. 
    // sc points to a SuperCat.
    // leftRank and rightRank designate which of the kbest parses from its
    //  children are used in the parse.
    // score is the aggregate score = score(sc) + score(left) + score(right)
    class kBestSC {
    public:
      const SuperCat *sc;

      mutable double leftRank;
      mutable double rightRank;

      mutable double score;

      kBestSC(){};
      ~kBestSC(){};

      kBestSC(const SuperCat *sc, double leftRank = -1, double rightRank = -1):
        sc(sc), leftRank(leftRank), rightRank(rightRank){};

      static bool compare(kBestSC a, kBestSC b) { return a.score > b.score; };
      
      bool operator==(kBestSC a) const { 
        return (a.sc == sc && a.leftRank == leftRank && a.rightRank == rightRank && a.score == score);
      };
      
      bool operator<(kBestSC a) const { 
        return (a.score < score);
      };
      
    };

  }
}