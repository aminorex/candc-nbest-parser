/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

namespace NLP {
  namespace CCG {

    class DecoderFactory: public Decoder {
    public:
      static void check(const std::string &name);
    private:
      Decoder *decoder;
    public:
      DecoderFactory(const std::string &name);
      virtual ~DecoderFactory(void){ delete decoder; }

      virtual double best_score(const SuperCat *sc){
	return decoder->best_score(sc);
      }
      const SuperCat *best_equiv(const SuperCat *sc){
	return decoder->best_equiv(sc);
      }
      virtual std::vector<kBestSC> *best(Chart &chart){
	return decoder->best(chart);
      }
      
      virtual std::vector<kBestSC> k_best_0_score(const SuperCat *sc, const double K){
	return decoder->k_best_0_score(sc, K);
      }
      std::vector<kBestSC> *k_best_0_equiv(const SuperCat *sc, const double K){
	return decoder->k_best_0_equiv(sc, K);
      }
      virtual std::vector<kBestSC> *k_best_0(Chart &chart){
	return decoder->k_best_0(chart);
      }   
      
      virtual std::vector<kBestSC> k_best_1_score(const SuperCat *sc, const double K){
        return decoder->k_best_1_score(sc, K);
      }
      std::vector<kBestSC> *k_best_1_equiv(const SuperCat *sc, const double K){
        return decoder->k_best_1_equiv(sc, K);
      }
      virtual std::vector<kBestSC> *k_best_1(Chart &chart){
        return decoder->k_best_1(chart);
      } 
      
      virtual kBestSC k_best_2_score(const SuperCat *sc, const double K){
        return decoder->k_best_2_score(sc, K);
      }
      std::vector<kBestSC> *k_best_2_equiv(const SuperCat *sc, const double K){
        return decoder->k_best_2_equiv(sc, K);
      }
      virtual std::vector<kBestSC> *k_best_2(Chart &chart){
        return decoder->k_best_2(chart);
      }
      virtual void k_best_3_getCandidates(const SuperCat *sc){
        decoder->k_best_3_getCandidates(sc);
      }
      virtual void k_best_3_lazynext(std::multiset<kBestSC> &cand, kBestSC kb, const double K){
        k_best_3_lazynext(cand, kb, K);
      }
      void k_best_3_lazy(const SuperCat *sc, double rank, const double K){
        k_best_3_lazy(sc, rank, K);
      }
      virtual std::vector<kBestSC> *k_best_3(Chart &chart){
        return decoder->k_best_3(chart);
      }
    };

  }
}
